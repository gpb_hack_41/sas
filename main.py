import conf
import os
import sys
import subprocess
import saspy
import argparse
from xml.etree import ElementTree
#from pprint import PrettyPrinter
#print=PrettyPrinter().pprint

def coalesce(*args):
    try: return next((arg for arg in args if arg is not None))
    except StopIteration: return None

parser = argparse.ArgumentParser(description='versioning script for GPB_Hack')
parser.add_argument('-f','--profile',type=str,default=None,help='SAS saved profile name')
parser.add_argument('--host',type=str,default=None,help='SAS metadata server address')
parser.add_argument('-p','--port',type=str,default=None,help='SAS metadata server port')
parser.add_argument('-u','--username',type=str,default=None,help='SAS username')
parser.add_argument('-s','--password',type=str,default=None,help="SAS user's password")
parser.add_argument('-d','--desc',type=str,default=None,help='Description label to filter metadata')
parser.add_argument('-n','--name_ext',type=str,default=None,help='External attribute name to filter metadata')
parser.add_argument('-x','--value_ext',type=str,default=None,help='External attribute value to filter metadata')
parser.add_argument('-t','--folder_tree',type=str,default=None,help='Root of folder tree  to filter metadata')
parser.add_argument('-o','--out_path',type=str,default=None,help='Output directory (with git repository)')
parser.add_argument('-v','--verbose',help='additional verbosity',action="store_true")
parser.add_argument('-c','--commit',help='do "git commit" after "git add"',action="store_true")
args = parser.parse_args()
#print([args.profile,args.host,args.username,args.password,args.desc,args.name_ext,args.value_ext,args.folder_tree,args.out_path,args.commit])

commit=args.commit
profile=coalesce(args.profile,os.getenv('PROFILE'),getattr(conf,'PROFILE',None))
host=coalesce(args.host,os.getenv('HOST'),getattr(conf,'HOST', None))
port=coalesce(args.port,os.getenv('PORT'),getattr(conf,'PORT',None))
user=coalesce(args.username,os.getenv('USER'),getattr(conf,'USER',None))
password=coalesce(args.password,os.getenv('PASS'),getattr(conf,'PASS',None))
if coalesce(profile,host and port and user and password) is None:
    print('No SAS profile or credentials found')
    exit(1)

cred_params = ('-profile', profile) if profile else ('-host',host,'-port',port,'-user',user,'-password',password)
cwd=os.getcwd()

desc=args.desc
desc_params = ('-includeDesc','-name',desc) if desc else []
ext_params = ('-extName',args.name_ext,*(('-extValue',args.value_ext) if args.value_ext else [])) if args.name_ext else []
tree_params = ('-folderTree', args.folder_tree) if args.folder_tree else []
out_path=coalesce(args.out_path,cwd,os.getenv('USERPROFILE'))
out_file=None
out_file=coalesce(out_file,'package.spk')
out_path_file=os.path.join(out_path,out_file)

obj_by_desc=[]
obj_by_ext=[]

if commit:
    cmd_git = subprocess.run('git status', capture_output=True, cwd=out_path)
    if (getattr(cmd_git,'returncode')!=0) or (len(getattr(cmd_git,'stderr','None'))!=0):
        print('Error checking git status:\n')
        print(getattr(cmd_git,'stderr',b'').decode('utf-8'))
        if args.verbose:
            print(getattr(cmd_git,'stdout',b'').decode('utf-8'))
        sys.exit(1)

# go to sas tools
os.chdir(conf.SAS_TOOLS_PATH)
cmd_meta = subprocess.run([conf.SAS_LIST_OBJECTS,*cred_params,'-format','xml',*tree_params,*desc_params,*ext_params]
                          ,capture_output=True
                          ,cwd=conf.SAS_TOOLS_PATH)
if len(cmd_meta.stderr)==0 and cmd_meta.returncode==0:
    elem_root=ElementTree.fromstring(cmd_meta.stdout.decode('utf-8'))
    obj_list=[obj.attrib for obj in elem_root if getattr(obj,'attrib',{}).get('Description',None)==desc
              ]if desc else [obj.attrib for obj in elem_root if getattr(obj,'attrib', None)]
else:
    print('Cant get metadata object list:\n')
    print(getattr(cmd_desc,'stderr',b'').decode('utf-8'))
    if args.verbose:
        print(getattr(cmd_desc, 'stdout', b'').decode('utf-8'))
    sys.exit(1)

#####

# lock objects
export_obj_list=sorted({obj['Path'] for obj in obj_list if 'Path' in obj},key=lambda x: ''.join((x[1],':',x[0])))
export_obj_list_quoted=list(map('"%s"'.__mod__, export_obj_list))

if len(export_obj_list_quoted) == 0:
    print('nothing to export')
    sys.exit(0)

sas = None
try:
    sas = saspy.SASsession(cfgfile=conf.SAS_CONFIG_FILE)
except:
    print('Error opening sas session to lock objects ...skipping')

if sas:
    res = sas.submit("""
        data work.tmp_input_paths;
            INFILE DATALINES DLM="^";
            length path $ 1000; 
            input path $;
            datalines;
        %s
        ;
        run;
        data work.results(keep=path id type);
            set work.tmp_input_paths;
            length id $20; length type $256;
            proj=""; deftype=""; id=""; type="";
            rc=metadata_pathobj(proj,path, deftype,type,id);
            put rc=; put id=;  put type=; put path=;
            if length(id)>7;
        run;
        proc print data=work.results; run;
    """%'\n'.join(export_obj_list), results='TEXT')
    if args.verbose:
        print(res['LOG'])
        #print(res['LST'])

    data = sas.sasdata('results','work')
    cont=data.to_frame()
    rows = cont.values.tolist()
    to_lock='\n'.join(map(lambda x:'<%s Id="%s"/>'%(x[2],x[1]),rows))

    f_out_locked = open(os.path.join(out_path,'locked_meta.csv'),'w+',encoding='utf-8')
    #f_out_locked.write('\n'.join([';'.join(['"%s"'%elem for elem in row]) for row in rows]))
    for row in rows:
        f_out_locked.write(';'.join(['"%s"'% str(elem) for elem in row]))
        f_out_locked.write('\n')
        f_out_locked.flush()
#    f_out_locked.flush()
    f_out_locked.close()
    f_out_locked_xml = open(os.path.join(out_path,'locked_meta.xml'),'w+')
    f_out_locked_xml.write(to_lock)
    f_out_locked_xml.flush()
    f_out_locked_xml.close()
    try:
        res = sas.submit("proc metadata in='<GetMetadata><Metadata>%s</Metadata><NS>SAS</NS><Flags>32768</Flags><Options/></GetMetadata>';run;"%to_lock)
    except e:
        print('Error:')
        print(res['LOG'])
        #print(res['LST'])
        sys.exit(1)

    if args.verbose:
        print(res['LOG'])
        # print(res['LST'])
    sas.disconnect()
#####

# check if can export
os.chdir(os.path.abspath(os.path.join(conf.SAS_TOOLS_PATH, os.pardir)))
cmd_import_test = subprocess.run(
    ' '.join([conf.SAS_EXPORT_PACKAGE, *cred_params,'-package',out_path_file,'-noexecute','-objects',*export_obj_list_quoted])
    ,capture_output=True)
cmd_import_test_res=cmd_import_test.stdout.decode('utf-8')
# print(cmd_import_test_res)
if cmd_import_test.returncode==0:
    # try export
    cmd_import = subprocess.run(
        ' '.join([conf.SAS_EXPORT_PACKAGE, *cred_params,'-package',out_path_file,'-objects',*export_obj_list_quoted])
        ,capture_output=True)
    cmd_import_res=cmd_import.stdout.decode('utf-8')
    # print(cmd_import_res)
    if cmd_import.returncode != 0 or args.verbose:
        print('Error while exporting package:')
        print(cmd_import.stderr.decode('utf-8'))
        if args.verbose:
            print(getattr(cmd_import,'stdout',b'').decode('utf-8'))
        exit(1)
else:
    print('Error while trying to export package:')
    print(cmd_import_test.stderr.decode('utf-8'))
    if args.verbose:
        print(getattr(cmd_import_test, 'stdout', b'').decode('utf-8'))
    exit(1)

# go to commit dir
os.chdir(out_path)
try:
    cmd_git_add=subprocess.run(['git','add',out_file,'locked_meta.xml','locked_meta.csv'], cwd=out_path)
    if commit:
        cmd_git_commit=subprocess.run('git commit -m "objects by filter: %s' % ' '.join([
            ('description=%s'%desc) if desc else '', ('ext.atr.nm=%s'%args.name_ext) if args.name_ext else '',
            ('ext.atr.val=%s'%args.value_ext) if args.value_ext else '',
            ('dir.folder_tree=%s'%args.folder_tree) if args.folder_tree else '']), cwd=out_path)
        if cmd_git_commit.returncode != 0 or args.verbose:
            print('Error while commiting package into git:')
            print(cmd_git_commit.stderr.decode('utf-8'))
            if args.verbose:
                print(getattr(cmd_git_commit, 'stdout', b'').decode('utf-8'))
            exit(1)
finally:
    pass
# go back to work dir
os.chdir(cwd)

"""
OMI_UNLOCK=131072 Unlocks an object lock that is held by the caller.
OMI_UNLOCK_FORCE=262144 Unlocks an object lock that is held by another user.
OMI_LOCK=32768  Locks the specified object and any associated objects selected by GetMetadata flags and options from update by everyone except the caller.
"""
# res = sas.submit("proc metadata in='<GetMetadata><Metadata>%s</Metadata><NS>SAS</NS><Flags>131072</Flags><Options/></GetMetadata>';run;" % to_lock)