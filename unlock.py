import conf
import os
import saspy
import argparse

def coalesce(*args):
    try: return next((arg for arg in args if arg is not None))
    except StopIteration: return None

parser = argparse.ArgumentParser(description='versioning script for GPB_Hack')
parser.add_argument('-o','--out_path',type=str,default=None,help='Output directory (with git repository)')
args = parser.parse_args()
cwd=os.getcwd()
out_path=coalesce(args.out_path,cwd,os.getenv('USERPROFILE'))
f_out_locked_xml = open(os.path.join(out_path,'locked_meta.xml'),'r+',encoding='utf-8')
to_lock = f_out_locked_xml.read()
f_out_locked_xml.close()
if len(to_lock) > 0:
    sas = saspy.SASsession(cfgfile=conf.SAS_CONFIG_FILE)
    sas.submit("proc metadata in='<GetMetadata><Metadata>%s</Metadata><NS>SAS</NS><Flags>131072</Flags><Options/></GetMetadata>';run;" % to_lock)
    sas.disconnect()